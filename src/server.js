var Model = require('./model/model')
var nodemailer = require('nodemailer')

// Promise
var nekretnine = require('./adapters/nekretnine')
var halo = require('./adapters/halo')

Promise.all([
  nekretnine,
  halo
]).then(function (value) {
  var stanovi = [].concat.apply([], value)

  var model = new Model('localhost', 27017)
  model
    .then(function (db) {
      var dbo = db.db('stanscraping');

      dbo.collection('stanovi').updateMany(
        {},
        { $set: {active: false} }
      ).then(function (res) {
          var promises = []
          for (var i = 0; i < stanovi.length; i++) {
            var stan = stanovi[i]

            var promise = dbo.collection('stanovi').updateOne(
                {id: stan.id},
                { $set: stan },
                {
                  upsert: true
                }
              )
                .then(function(updateDoc){
                  if(updateDoc.upsertedId) {
                      return dbo.collection('stanovi').findOne({
                        _id: updateDoc.upsertedId._id
                      }).then(function (result) {
                        return result
                      })
                  }
                })

            promises.push(promise)
          }

          Promise.all(promises)
            .then(function (noviStanovi) {
              var html = '<ul>'
              var broj = 1
              for (var i = 0; i < noviStanovi.length; i++) {
                var stan = noviStanovi[i]
                if (stan) {
                  html += '<li>' + broj + '. <a href="' + stan.link + '">' + stan.title + '</a></li>'
                  broj++
                }
              }
              html += '</ul>'
              var date = new Date().toISOString()
                .replace(/T/, ' ')
                .replace(/\..+/, '')
              if (broj > 1) {
                sendMail('Novi stanovi - ' + date, html)
              }
              db.close()
              // console.log('aaa', process._getActiveHandles().length)
          }).catch(function (reason) {
            console.log('error', reason)
            db.close()
          })
        })
        .catch(function () {
          db.close()
        })
    })
})

function sendMail(title, message) {
  // create reusable transporter object using the default SMTP transport
  var transporter = nodemailer.createTransport({
    host: 'smtp-mail.outlook.com',
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: 'damjanbu@outlook.com', // generated ethereal user
      pass: 'Erste.56mGate' // generated ethereal password
    }
  });

  // setup email data with unicode symbols
  var mailOptions = {
    from: '"Fred Foo 👻" <damjanbu@outlook.com>', // sender address
    to: 'bursacdamjan@gmail.com, vesovicana@gmail.com',
    subject: title, // Subject line
    // text: 'Hello world?', // plain text body
    html: '<b>Pojavio se novi stan na adresi: </b> <br/><br/>' + message // html body
  };

  // send mail with defined transport object
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      return console.log(error);
    }
    console.log('Message sent: %s', info.messageId);
    // Preview only available when sending through an Ethereal account
    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
  });
}

// Create/open database
// new Model('localhost', 27017)
