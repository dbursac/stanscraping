var Db = require('mongodb').Db
var Connection = require('mongodb').Connection
var Server = require('mongodb').Server
var BSON = require('mongodb').BSON
var ObjectID = require('mongodb').ObjectID

var MongoClient =require('mongodb').MongoClient

function Model (host, port) {
  console.log('new Model')
  var url = 'mongodb://' + host + ':' + port
  return MongoClient.connect(url)
  // MongoClient.connect(url, function (err, db) {
  //   if (err) throw err;
  //   var dbo = db.db('stanscraping')
  //   dbo.createCollection('stanovi', function (err, res) {
  //     if (err) throw err;
  //     console.log('Collection is created!');
  //     db.close();
  //   })
  // })
}

Model.prototype.create = function (data, callback) {
  this.getCollection(function (error, collection) {
    if (error) {
      callback(error)
    } else {
      if (typeof data === 'undefined') {
        data = [data]
      }

      for (var i = 0; i < data.length; i++) {
        item = data[i]
        item.created_at = new Date()
      }

      collection.insert(data, function(){
        callback(null, data)
      })
    }
  })
}

Model.prototype.read = function () {

}

Model.prototype.update = function () {

}

Model.prototype.updateMany = function () {
  this.dbo.collection('stanovi').insertMany(merged, function (err, res) {
    if (err) throw err;
    console.log("Number of documents inserted: " + res.insertedCount);
    db.close();
  })
}

Model.prototype.delete = function () {

}

module.exports = Model