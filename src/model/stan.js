/**
 *
 * @param id
 * @param title
 * @param price
 * @param surface
 * @param link
 * @param {string} date - dd.mm.yyyy
 * @returns {{id: *, title: *, price: *, surface: *, link: *, date: *}}
 * @constructor
 */
function Stan (id, title, price, surface, link, date, site) {
  return {
    id: id,
    title: title,
    price: price,
    surface: surface,
    link: link,
    date: date.split('.').reverse().join('-'),
    site: site,
    active: true
  }
}

module.exports = Stan
