var axios = require('axios')
var cheerio = require('cheerio')
var Stan = require('../model/stan')

module.exports = getItems(1)

function getItems(stranica) {
  return getItemsFragment(stranica)
    .then(function (data) {
      if (data.length === 20) {
        stranica++
        return getItems(stranica)
          .then(function(nextData) {
            return data.concat(nextData)
          })
      } else {
        return data
      }
    })
}

function getItemsFragment(stranica) {
  if (!stranica) {
    stranica = 1
  }

  var stranicaUrl = ''

  if (stranica > 1) {
    stranicaUrl = '&page=' + stranica
  }

  var resource = 'https://www.halooglasi.com/nekretnine/prodaja-stanova?grad_id_l-lokacija_id_l-mikrolokacija_id_l=52205%2C56569%2C58067%2C321814%2C57245&kvadratura_d_from=55&kvadratura_d_to=90&kvadratura_d_unit=1' + stranicaUrl

  return axios.get(resource, {
    headers: {
      'Content-Type': 'text/html'
    }
  })
    .then(function (response) {
      var $ = cheerio.load(response.data)

      var listProductSelector = '.product-item.real-estates'

      var title, price, surface, link, date
      var json = [] // {id: "", title: "", price: "", surface: "", link: "", date: ""}

      $(listProductSelector).filter(function () {
        var data = $(this)

        var id = data.data('id').toString()

        var wrapTitleLink = data.find('h3.ad-title').children('a')

        var link = 'https://www.halooglasi.com' + wrapTitleLink.attr('href')
        var title = wrapTitleLink.text()
        var price = data.children('.central-feature').children('span').data('value')
        var surfaceWrap = data.find('ul.ad-features li:nth-child(2)').children().text()
        var surface = surfaceWrap.match(/\d+/)[0]

        var date = data.find('span.publish-date').text()

        var stan = new Stan(id, title, price, surface, link, date, 'Halo Oglasi')

        json.push(stan)
      })

      return json
    })
}
