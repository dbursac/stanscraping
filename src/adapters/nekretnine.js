var axios = require('axios')
var cheerio = require('cheerio')
var Stan = require('../model/stan')

module.exports = getItems(1)

function getItems(stranica) {
  return getItemsFragment(stranica)
    .then(function (data) {
      if (data.length === 50) {
        stranica++
        return getItems(stranica)
          .then(function(nextData) {
            return data.concat(nextData)
          })
      } else {
        return data
      }
    })
}

function getItemsFragment(stranica) {
  if (!stranica) {
    stranica = 1
  }

  var stranicaUrl = ''

  if (stranica > 1) {
    stranicaUrl = '/stranica/' + stranica
  }

  var resource = 'https://www.nekretnine.rs/stambeni-objekti/stanovi/izdavanje-prodaja/prodaja/zemlja/srbija/vrsta-grejanja/centralno-grejanje/grad/beograd/deo-grada/blok-11b_blok-11c-stari-merkator_blok-7-paviljoni_blok-8-paviljoni_blok-8a-paviljoni_blok-9a-dom-zdravlja_retenzija_hotel-jugoslavija-blok-11/kvadratura/55_90/lista/po_stranici/50' + stranicaUrl

  return axios.get(resource, {
    headers: {
      'Content-Type': 'text/html'
    }
  })
    .then(function (response) {
      var $ = cheerio.load(response.data)

      var title, price, surface, link, date
      var json = [] // {id: "", title: "", price: "", surface: "", link: "", date: ""}

      $('.resultList').filter(function () {
        var data = $(this)
        var id = data.attr('id').toString()
        var resultInfo = data.children('.resultInfo')
        var title = resultInfo.children('h2').children().text()
        var link = resultInfo.children('h2').children().attr('href')

        var priceSurface = resultInfo.children('.resultOtherWrap').children('.resultListPrice').text()
        var pirceSurfaceArr = priceSurface.split(',')
        var surface = pirceSurfaceArr[0].trim()
        var price = pirceSurfaceArr[1].trim()

        var date = data.find('div.timeCat').text().split(' | ')[0].trim()
        var stan = new Stan(id, title, price, surface, link, date, 'Nekretnine')

        json.push(stan)
      })

      return json
    })
}
